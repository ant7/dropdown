/**
 * Manage opening and closing of dropdowns (default use) and tooltips
 * @requires Mustache
 */

class Dropdown {
    /**
     * Constructor
     * @param {Element} element
     * @param {Object} options
     */
    constructor(element, options = {}) {
        this.element = element;
        this.options = options;
        this.id = element.id;

        this.element.Dropdown = this;

        this.placeholderElement = document.querySelector('[data-' + this.options.namespace + '-placeholder]');

        if (!this.placeholderElement) {
            this.createPlaceholderElement();
        }

        this.templateElement = this.element.querySelector('template');
        if (this.templateElement) {
            this.template = this.templateElement.innerHTML;
        }

        this.registerEvents();
    }


    /**
     * Register events
     */
    registerEvents() {
        document.body.addEventListener('click', delegate('[href^="#' + this.id + '"], [data-' + this.options.namespace + '="' + this.id + '"]', (e) => {
            e.preventDefault();
            this.open(e.delegateTarget);
        }));

        document.body.addEventListener('click', (e) => {
            if (this.isElementIncludedInTarget(e.target)) return;
            this.close();
        });

        window.addEventListener('resize', (e) => this.position());
    }


    /**
     * Create placeholder element
     */
    createPlaceholderElement() {
        if (this.placeholderElement) return;
        this.placeholderElement = document.createElement('div');
        this.placeholderElement.setAttribute('data-' + this.options.namespace + '-placeholder', true);
        document.body.appendChild(this.placeholderElement);
    }


    /**
     * Register observer
     * Attach a mutation observer to the original element, so that the changes
     * made in it are reflected in the cloned element, if opened
     */
    observeElement() {
        this.observer = this.observer ? this.observer : new MutationObserver(this.observeElementMutations);
        this.observer.observe(this.element, {
            attributes: true,
            childList: true,
            subtree: true
        });
    }

    /**
     * Is Opened
     * @return {Boolean}
     */
    get isOpened() {
        return (this.elementClone && this.elementClone.classList.contains(this.options.namespace + '--opened'));
    }


    /**
     * Open
     * @param {Element} triggerElement
     */
    open(triggerElement) {
        if (this.isOpened) {
            return this.close();
        }

        let data = getValidJSON(triggerElement.getAttribute('data-' + this.options.namespace + '-data'));

        this.currentTrigger = triggerElement;
        this.elementClone = (this.elementClone ? this.elementClone : this.element.cloneNode(true));
        this.render(data);

        this.elementClone.classList.add(this.options.namespace + '--opened');
        this.currentTrigger.setAttribute('aria-expanded', true);

        this.observeElement();
        this.placeholderElement.appendChild(this.elementClone);
        this.position();

        this.currentTrigger.classList.add(this.options.namespace + '--isopened');
    }

    /**
     * Close
     */
    close() {
        if (!this.isOpened) return;

        this.currentTrigger.classList.remove(this.options.namespace + '--isopened');
        this.elementClone.classList.remove(this.options.namespace + '--opened');
        this.currentTrigger.setAttribute('aria-expanded', false);


        this.observer.disconnect();
        setTimeout(() => {
            this.elementClone.remove();
        }, 100);
    }



    /**
     * Render template
     * @param Object data
     */
    render(data = {}) {
        if (!this.template || !data || typeof data !== 'object' || (typeof data === 'object' && !Object.keys(data).length)) return false;

        while (this.elementClone.firstChild) {
            this.elementClone.removeChild(this.elementClone.firstChild);
        }

        this.elementClone.innerHTML = Mustache.render(this.template, data);

        return true;
    }

    /**
     * Is element included in current target
     * @param Element click
     * @return {Boolean}
     */
    isElementIncludedInTarget(targetElement) {
        if (!this.isOpened) return false;

        let isCloseOnSelectFulfilled = (this.currentTrigger.hasAttribute('data-' + this.options.namespace + '-closeonselect') &&
            (this.elementClone.contains(targetElement)) &&
            ['BUTTON', 'A', 'INPUT', 'LABEL'].includes(targetElement.nodeName));

        if (isCloseOnSelectFulfilled) {
            return false;
        }

        if (this.currentTrigger.contains(targetElement) || this.elementClone.contains(targetElement) || this.element.contains(targetElement)) {
            return true;
        }

        return false;
    }

    /**
     * Position the popover according to the trigger element
     */
    position() {
        if (!this.isOpened) return;

        let position = this.getPosition();
        this.elementClone.style.transform = 'translateX(' + position.left + 'px) translateY(' + position.top + 'px)';

        // remove previous alignment classes
        this.elementClone.classList.remove(this.options.namespace + '--left');
        this.elementClone.classList.remove(this.options.namespace + '--right');

        if (position.alignment) {
            this.elementClone.classList.add(this.options.namespace + '--' + position.alignment);
        }
    }


    /**
     * Get position relative to an element
     * @return Object positionData
     */
    getPosition() {
        let positionData = {};

        let width = this.elementClone.offsetWidth;
        let height = this.elementClone.offsetHeight;

        let boxData = this.currentTrigger.getBoundingClientRect();
        let offset = getValidJSON(this.currentTrigger.getAttribute('data-' + this.options.namespace + '-offset')) || this.options.offset;

        let left = this.elementClone.classList.contains(this.options.namespace + '--center') ?
            boxData.left + window.scrollX + (offset ? offset.x : 0) - width / 2 + boxData.width / 2 :
            boxData.left + window.scrollX + (offset ? offset.x : 0);
        let top = this.elementClone.classList.contains(this.options.namespace + '--top') ?
            boxData.top + window.scrollY - height - (offset ? offset.y : 0) :
            boxData.top + window.scrollY + boxData.height + (offset ? offset.y : 0);

        // Minimum horizontal collision detection
        if (left + width > document.body.clientWidth) {
            left = Math.max(0, ((boxData.left + window.scrollX + boxData.width) - width));
            positionData.alignment = 'right';
        } else if (left < 0) {
            left = boxData.left + window.scrollX - (offset ? offset.x : 0);
            positionData.alignment = 'left';
        }

        positionData.left = left;
        positionData.top = top;

        return positionData;
    }


    /**
     * Observe mutations in the original markup element
     * @param {Object} mutationsList
     */
    observeElementMutations(mutationsList) {
        for (let mutation of mutationsList) {
            this.updateContents(mutation.target.closest('.' + this.options.namespace).innerHTML, this.currentTrigger);
        }
    }


    /**
     * Update contents of the clone element
     * @param {String} contents
     * @param {Element|false} triggerElement
     */
    updateContents(contents, triggerElement = false) {
        this.elementClone.innerHTML = contents;
        if (triggerElement) {
            this.position(triggerElement);
        }
    }

    /**
     * Delegate event
     * @param String selector
     * @param Function listener
     * @returns Function
     */
    delegate(selector, listener) {
        return function(e) {
            let element = e.target;
            let originalTarget = e.currentTarget;
            let relatedTarget = e.relatedTarget;

            do {
                if (!(element instanceof Element) || !element.matches(selector)) {
                    continue;
                }

                e.delegateTarget = element;
                listener.apply(this, arguments);
                return;
            } while (element = element.parentNode)
        };
    }

}

/**
 * Tooltip
 * @extends Dropdown
 */
class Tooltip extends Dropdown {

    /**
     * Register events
     */
    registerEvents() {
        document.body.addEventListener('mouseover', delegate('[href^="#' + this.id + '"], [data-tooltip="' + this.id + '"]', (e) => {
            e.preventDefault();
            this.open(e.delegateTarget);
        }));

        document.body.addEventListener('touchenter', delegate('[href^="#' + this.id + '"], [data-tooltip="' + this.id + '"]', (e) => {
            e.preventDefault();
            this.open(e.delegateTarget);
        }), {passive: true});

        // toooltip role - close
        document.body.addEventListener('mouseout', delegate('[href^="#' + this.id + '"], [data-tooltip="' + this.id + '"]', (e) => {
            e.preventDefault();
            this.close();
        }));

        document.body.addEventListener('touchleave', delegate('[href^="#' + this.id + '"], [data-tooltip="' + this.id + '"]', (e) => {
            e.preventDefault();
            this.close();
        }), {passive: true});

        window.addEventListener('resize', (e) => this.position());
    }


    /**
     * Get position relative to an element
     * @return Object positionData
     */
    getPosition() {
        let positionData = {};

        let width = this.elementClone.offsetWidth;
        let height = this.elementClone.offsetHeight;

        let boxData = this.currentTrigger.getBoundingClientRect();
        let offset = getValidJSON(this.currentTrigger.getAttribute('data-tooltip-offset')) || this.options.offset;

        let left = boxData.left + window.scrollX - width / 2 + boxData.width / 2;
        let top = boxData.top + window.scrollY - height - (offset ? offset.y : 0);

        // Minimum horizontal collision detection
        if (left + width > document.body.clientWidth) {
            left = Math.max(0, ((boxData.left + window.scrollX + boxData.width) - width));
            positionData.alignment = 'right';
        } else if (left < 0) {
            left = boxData.left + window.scrollX - (offset ? offset.x : 0)
            positionData.alignment = 'left';
        }

        positionData.left = left;
        positionData.top = top;

        return positionData;
    }
}


class DropdownHandler {
    constructor() {
        let dropdownElements = document.querySelectorAll('.dropdown');
        Array.from(dropdownElements).forEach((element) => {
            if (element.Dropdown) return;
            new Dropdown(element, {
                namespace: 'dropdown'
            });
        });

        let tooltipElements = document.querySelectorAll('.tooltip');
        Array.from(tooltipElements).forEach((element) => {
            if (element.Popover) return;
            new Tooltip(element, {
                namespace: 'tooltip'
            });
        });
    }
}

new DropdownHandler();
